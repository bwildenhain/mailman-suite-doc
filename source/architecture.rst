=============================
Architecture of Mailman Suite
=============================

Mailman Suite is a set of two functional components, Mailman Core and Web
Frontend. Web frontend is based on Django Web Framework and is often just
referred to as Django. The actual projects that use the Django web framework
are called Postorius (Web Frontend) and Hyperkitty (Archiver). From a runtime
perspective, Postorius and Hyperkitty can run in a single component.


.. image:: _static/mailman-architecture.png
   :width: 800px
   :alt: Mailman Suite Architecture


* **MTA**: Mail-Transfer-Agent is typically responsible for receiving emails
  from internet and sending out emails. It is configured to forward emails to
  Mailman owned mailing lists to Mailman Core's LMTP server via LMTP protocol.

  MTA also receives incoming requests to send out emails from both Mailman Core
  and Web Interface. Web interface will send out emails related to account
  management like verifying email addresses for example.

  Hyperkitty also allows replying to posts from the web interface, which are
  sent out as emails through the MTA, which then in-turn are forwarded to
  Mailman Core like any other incoming email.

* **Web Server**: Web server is typically just a frontend to Web Interface due
  to security and performance. It is configured as a reverse-proxy for the Web
  Interface.

  Web Fronend will support any web server that can do reverse proxy.

* **Mailman Core**: Mailman Core is a complex application and the above diagram
  only represents some parts of it. It listens primarily on two ports:

  * **REST API**: This is where Mailman's administrative REST API listens. This
    is used by front-end to retrive and show information to the user.

  * **LMTP Server**: This is how Mailman receives incoming emails from MTA.

* **Web Interface**: This includes both Hyperkitty and Postorius.

  * **Hyperkitty**: This receives emails that are to be archived from
    mailman-hyperkitty plugin for Mailman Core. It also hosts the archives
    for the mailing lists.

  * **Postorius**: This presents an interface to view/manage mailing lists
    and subscriptions. It also has various templates for mailing lists, which
    users can create to override the default ones provided by Mailman. Mailman
    Core will reach out to Postorius to fetch the templates when sending out
    emails.
